from django.db import models

from ManageDepartments import models as DM

# Create your models here.


class Course(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    department = models.ForeignKey(DM.Department, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'course'
