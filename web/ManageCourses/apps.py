from django.apps import AppConfig


class ManagecoursesConfig(AppConfig):
    name = 'ManageCourses'
