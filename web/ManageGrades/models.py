from django.db import models
from MenageUsers import models as MU
from ManageSubjects import models as MS
# Create your models here.


class Grade(models.Model):
    value = models.IntegerField()
    student = models.ForeignKey(MU.Student, models.DO_NOTHING)
    subject = models.ForeignKey(MS.Subject, models.DO_NOTHING)

    def __str__(self):
        return self.subject

    class Meta:
        managed = False
        db_table = 'grade'
