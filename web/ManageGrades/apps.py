from django.apps import AppConfig


class ManagegradesConfig(AppConfig):
    name = 'ManageGrades'
