from django.apps import AppConfig


class MenageusersConfig(AppConfig):
    name = 'MenageUsers'
