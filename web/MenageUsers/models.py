from django.db import models
from ManageDepartments import models as MD
# Create your models here.


class Lecturer(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    department = models.ForeignKey(MD.Department, models.DO_NOTHING)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'lecturer'


class Student(models.Model):
    id = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'student'
