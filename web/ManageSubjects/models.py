from django.db import models
from ManageCourses import models as MC
from MenageUsers import models as MU

# Create your models here.
class Term(models.Model):
    course = models.ForeignKey(MC.Course, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'term'


class StudentTerm(models.Model):
    student = models.ForeignKey(MU.Student, models.DO_NOTHING)
    term = models.ForeignKey(Term, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'student_term'


class Subject(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'subject'


class SubjectLecturer(models.Model):
    lecturer = models.ForeignKey(MU.Lecturer, models.DO_NOTHING)
    subject = models.ForeignKey(Subject, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'subject_lecturer'




class SubjectTerm(models.Model):
    term = models.ForeignKey(Term, models.DO_NOTHING)
    subject = models.ForeignKey(Subject, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'subject_term'
