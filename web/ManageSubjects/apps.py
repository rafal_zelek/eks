from django.apps import AppConfig


class ManagesubjectsConfig(AppConfig):
    name = 'ManageSubjects'
