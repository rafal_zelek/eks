from django.apps import AppConfig


class ManagedepartmentsConfig(AppConfig):
    name = 'ManageDepartments'
