from django.db import models

# Create your models here.


class Department(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'department'
