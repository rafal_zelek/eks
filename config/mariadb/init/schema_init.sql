CREATE DATABASE  IF NOT EXISTS DJANGO;

GRANT ALL PRIVILEGES ON DJANGO.* TO 'DJANGO'@'localhost' IDENTIFIED BY 'DJANGO'; FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON DJANGO.* TO 'DJANGO'@'%' IDENTIFIED BY 'DJANGO' WITH GRANT OPTION; FLUSH PRIVILEGES;

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema DJANGO
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema DJANGO
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `DJANGO` DEFAULT CHARACTER SET utf8 ;
USE `DJANGO` ;

-- -----------------------------------------------------
-- Table `DJANGO`.`department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`department` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`lecturer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`lecturer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `department_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_lecturer_department1_idx` (`department_id` ASC),
  CONSTRAINT `fk_lecturer_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `DJANGO`.`department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`student` (
  `id` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`subject` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`grade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`grade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `value` TINYINT NOT NULL,
  `student_id` INT NOT NULL,
  `subject_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_grade_student1_idx` (`student_id` ASC),
  INDEX `fk_grade_subject1_idx` (`subject_id` ASC),
  CONSTRAINT `fk_grade_student1`
    FOREIGN KEY (`student_id`)
    REFERENCES `DJANGO`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grade_subject1`
    FOREIGN KEY (`subject_id`)
    REFERENCES `DJANGO`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`course` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `department_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_course_department1_idx` (`department_id` ASC),
  CONSTRAINT `fk_course_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `DJANGO`.`department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`term`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`term` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `course_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_term_course1_idx` (`course_id` ASC),
  CONSTRAINT `fk_term_course1`
    FOREIGN KEY (`course_id`)
    REFERENCES `DJANGO`.`course` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`student_term`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`student_term` (
  `student_id` INT NOT NULL,
  `term_id` INT NOT NULL,
  INDEX `fk_student_department_student1_idx` (`student_id` ASC),
  INDEX `fk_student_department_term1_idx` (`term_id` ASC),
  CONSTRAINT `fk_student_department_student1`
    FOREIGN KEY (`student_id`)
    REFERENCES `DJANGO`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_department_term1`
    FOREIGN KEY (`term_id`)
    REFERENCES `DJANGO`.`term` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`subject_term`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`subject_term` (
  `term_id` INT NOT NULL,
  `subject_id` INT NOT NULL,
  INDEX `fk_subject_term_term1_idx` (`term_id` ASC),
  INDEX `fk_subject_term_subject1_idx` (`subject_id` ASC),
  CONSTRAINT `fk_subject_term_term1`
    FOREIGN KEY (`term_id`)
    REFERENCES `DJANGO`.`term` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_subject_term_subject1`
    FOREIGN KEY (`subject_id`)
    REFERENCES `DJANGO`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `DJANGO`.`subject_lecturer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DJANGO`.`subject_lecturer` (
  `lecturer_id` INT NOT NULL,
  `subject_id` INT NOT NULL,
  INDEX `fk_subject_lecturer_lecturer1_idx` (`lecturer_id` ASC),
  INDEX `fk_subject_lecturer_subject1_idx` (`subject_id` ASC),
  CONSTRAINT `fk_subject_lecturer_lecturer1`
    FOREIGN KEY (`lecturer_id`)
    REFERENCES `DJANGO`.`lecturer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_subject_lecturer_subject1`
    FOREIGN KEY (`subject_id`)
    REFERENCES `DJANGO`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;