FROM python:3.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /web
WORKDIR /web
ADD pip_dependencies /web/
RUN pip install -r pip_dependencies
ADD web /web

EXPOSE 8000
CMD ["python", "manage.py", "migrate"]
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]